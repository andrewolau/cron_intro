#!/bin/bash
PATH=/home/alau3/.conda/envs/myenv2/bin:/opt/miniconda/condabin:/home/alau3/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/opt/miniconda/bin
echo ">>> running query on Big Query"
source activate myenv2
python /home/alau3/chapter/cron_intro/example_2/query.py
echo ">>> ...done"
echo ">>> copying over to GCP bucket"
gsutil cp /home/alau3/chapter/cron_intro/example_2/query_output.txt gs://wx-personal/AndrewLau/chapter/cron_training/query_output.txt
echo ">>> ...done"


