# create environemtn
#! conda create -n myenv2 ipython
#! source activate myenv2
# install GCP packages
#! pip install google-cloud-bigquery
# authenticate
#!gcloud auth login
from google.cloud import bigquery
import os
os.environ["GCLOUD_PROJECT"] = "wx-bq-poc"
os.environ['GOOGLE_APPLICATION_CREDENTIALS']="/home/alau3/.config/gcloud/legacy_credentials/alau3@woolworths.com.au/adc.json" 

client = bigquery.Client()

QUERY = (
    "SELECT cmpgn_code, description, cmpgn_type, business_unit, lifecycle_start_dt FROM `wx-bq-poc.loyalty.campaign` WHERE lifecycle_start_dt < '2100-01-01' ORDER BY lifecycle_start_dt DESC limit 10;")
query_job = client.query(QUERY)  # API request
rows = query_job.result()  # Waits for query to finish

f = open("/home/alau3/chapter/cron_intro/example_2/query_output.txt", "w")
for row in rows:
    print(row[0:5])
    f.write(str(row[0:5]) + "\n")

f.close()


